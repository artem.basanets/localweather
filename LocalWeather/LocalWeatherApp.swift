//
//  LocalWeatherApp.swift
//  LocalWeather
//
//  Created by Artem Basanets on 12/17/21.
//

import SwiftUI

@main
struct LocalWeatherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
