//
//  ContentView.swift
//  LocalWeather
//
//  Created by Artem Basanets on 12/17/21.
//

import SwiftUI

struct ContentView: View {
    
    
    @StateObject var locationManager = LocationManager()
    
    @State var results = [WeatherResponse]()
    
    @State var resultObj : WeatherResponse? = nil
    
    
    var latitude: String {
        return "\(locationManager.lastLocation?.coordinate.latitude ?? 0)"
    }
    
    var longitude: String {
        return "\(locationManager.lastLocation?.coordinate.longitude ?? 0)"
    }
    
    var cityName: String {
        return "\(locationManager.currentPlacemark?.administrativeArea ?? "unknown")"
    }
    
    
    var body: some View {
        VStack() {
            Text("Hello SwiftUI!")
            Text("current temp. " + numtostr(x: resultObj?.current.tempC))
                .onAppear(perform: loadData)
            Text("lat: \(latitude) " + "lon: \(longitude)")
                .font(.system(size: 28, weight: .light, design: .default))
                .foregroundColor(Color.red)
                .multilineTextAlignment(.center)
            HStack {
                Text("administrative area: ")
                Text("\(cityName)")
                    .font(.system(size: 20, weight: .medium, design: .default))
            }
            Text("status: \(locationManager.statusString)")
        }
    }
    
    
    func numtostr(x:Double?) -> String{
        return x != nil ? String(x!) : ""
    }
    
    func loadData() {
            guard let url = URL(string: "https://api.weatherapi.com/v1/current.json?key=7c61d045232b4ba085214240211912&q=Cracow") else {
                print("Invalid URL")
                return
            }
            let request = URLRequest(url: url)
            URLSession.shared.dataTask(with: request) { data, response, error in
                DispatchQueue.main.async {
                    let weatherResponse = try! JSONDecoder().decode(WeatherResponse.self, from: data!)
                    self.results.append(weatherResponse)
                    self.resultObj = weatherResponse
                }
            }.resume()
        }
}
